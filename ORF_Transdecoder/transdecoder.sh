#!/bin/bash
#$ -N Transcoder_job
#$ -M nasim.rahmatpour@uconn.edu
#$ -m bea 
#$ -S /bin/bash
#$ -q highmem.q
#$ -cwd            # tells GE to execute the job from  the  current  working  directory
#$ -pe smp 8
#$ -o Transcoder_$JOB_ID.out
#$ -e Transcoder_$JOB_ID.err

/common/opt/bioinformatics/TransDecoder-2.0.1/TransDecoder.LongOrfs -t clustered.filteredHeader.fasta
hmmscan --cpu 8 --domtblout pfam.domtblout /common/Pfam/v31.0/Pfam-A.hmm clustered.filteredHeader.fasta.transdecoder_dir/longest_orfs.pep
/common/opt/bioinformatics/TransDecoder-2.0.1/TransDecoder.Predict -t clustered.filteredHeader.fasta --retain_pfam_hits pfam.domtblout
