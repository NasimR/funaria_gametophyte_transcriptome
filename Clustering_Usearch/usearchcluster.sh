#!/bin/bash
#$ -N usearchcluster_job
#$ -M nasim.rahmatpour@uconn.edu
#$ -m bea
#$ -S /bin/bash
#$ -q all.q
#$ -cwd
#$ -pe smp 4
#$ -o usearchcluster_$JOB_ID.out
#$ -e usearchcluster_$JOB_ID.err

module load usearch
usearch -cluster_fast cat.G1G2G3_filtered.fasta -id 0.90 -threads 4 -centroids clustered.filtered.fasta


