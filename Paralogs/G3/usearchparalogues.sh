#!/bin/bash
#$ -N usearchcluster_job
#$ -M nasim.rahmatpour@uconn.edu
#$ -m bea
#$ -S /bin/bash
#$ -q all.q
#$ -cwd
#$ -pe smp 4
#$ -o usearchcluster_$JOB_ID.out
#$ -e usearchcluster_$JOB_ID.err

module load usearch
usearch -cluster_fast G3_fpkm_filtered.fasta.transdecoder.cds -id 0.80 -threads 4 -centroids nr.fasta -uc clusters.uc


