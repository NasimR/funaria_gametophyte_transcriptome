#!/bin/bash
#SBATCH --job-name=contam_removal
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o contam_removal_%j.out
#SBATCH -e contam_removal_%j.err

python contam_removal.py -c /home/CAM/nrahmatpour/Nasim/Haploid.Gametophyte/nr.enTAP/no_internal/python_contam_removal/best_hits_contam.faa  -f /home/CAM/nrahmatpour/Nasim/Haploid.Gametophyte/nr.enTAP/no_internal/python_contam_removal/clustered.filteredHeader.fasta.transdecoder.pep_alt
