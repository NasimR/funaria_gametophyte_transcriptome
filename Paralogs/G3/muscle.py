#!/usr/bin/env python

import glob, os

muscle_options = " -maxiters 1 -diags1"

for fasta_file in glob.glob("*.fasta"):
    afa_file = fasta_file + ".afa"
    command = "muscle -in %s -out %s %s" % (fasta_file, afa_file, muscle_options)
    print "executing: " + command
    os.system(command)

