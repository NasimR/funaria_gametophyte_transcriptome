#!/bin/bash
#$ -N star_job
#$ -M nasim.rahmatpour@uconn.edu
#$ -m bea
#$ -S /bin/bash
#$ -cwd
#$ -pe smp 8 
#$ -o star_$JOB_ID.out
#$ -e star_$JOB_ID.err

STAR --genomeDir /common/data/physcomitrella3.3/Ppatens3.3_STAR/ --readFilesIn /archive/Nasim/NextSeq/cat.G1.R1.fastq /archive/Nasim/NextSeq/cat.G1.R2.fastq --runThreadN 8 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix physcomitrellamapG1 
STAR --genomeDir /common/data/physcomitrella3.3/Ppatens3.3_STAR/ --readFilesIn /archive/Nasim/NextSeq/cat.G2.R1.fastq /archive/Nasim/NextSeq/cat.G2.R2.fastq --runThreadN 8 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix physcomitrellamapG2
STAR --genomeDir /common/data/physcomitrella3.3/Ppatens3.3_STAR/ --readFilesIn /archive/Nasim/NextSeq/cat.G3.R1.fastq /archive/Nasim/NextSeq/cat.G3.R2.fastq --runThreadN 8 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix physcomitrellamapG3

