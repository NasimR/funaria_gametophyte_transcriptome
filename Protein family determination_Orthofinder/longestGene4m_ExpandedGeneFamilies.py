#!/usr/bin/env python

                ##################################################################################
                ## Neranjan V Perera (2018 March 8)                                             ##
                ## Computational Biology Core                                                   ##
                ## Institute for Systems Genomics                                               ##
                ## University of Connecticut                                                    ##
                ## Copyright                                                                    ##
                ##                                                                              ##
		## The program will check the orthogroup family for focused species (TAG) 	##
		## which is represented more than the specified number using --no tag		##
		## From the orthogroup families which have been over represented more than the	##
		## number selected, it will find the longest gene sequence and will write 	##
		## it into a fasta file.							##
		##										##
		## arguments:									##
		## 	python longestGene4m_ExpandedGeneFamilies.py 				##
		##		--orthofile Orthogroups.txt					##
		##		--fasta Funaria.fasta						##
		##		--tag TR							##
		##		--no 6								##
		##		--outfile Funaria_orthogroup.fasta				##
		##										##
		################################################################################## 


import sys,re,argparse
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord


def info():
        print ("""
                ##################################################################################
                ## Neranjan V Perera (2018 March 8)                                             ##
                ## Computational Biology Core                                                   ##
                ## Institute for Systems Genomics                                               ##
                ## University of Connecticut                                                    ##
                ## Copyright                                                                    ##
                ##                                                                              ##
                ## The program will check the orthogroup family for focused species (TAG)       ##
                ## which is represented more than the specified number using --no tag           ##
                ## From the orthogroup families which have been over represented more than the  ##
                ## number selected, it will find the longest gene sequence and will write       ##
                ## it into a fasta file.                                                        ##
                ##                                                                              ##
                ##################################################################################
        """)
        return


'''
gets the line containing the gene_Tag and filters out the corrosponding genes 
where only the orthogroups which contains species genes are selected.
[exclusive orthogroup genes for the Tag species will be selected]
input:
	ln = orthogroup line containing genes
	sp_tag = species TAG
	tag_no = number of the TAG species to consider in the orthogroup for it to consider
	
output 
	gene list (count) of the species 
'''
def orthogroup_expandedGenesInTagSpecies(ln,sp_tag,tag_no):
        gene_id_list = ""
        passing_gene_id_list = ""
        tag="^"+sp_tag

	#get the ortholog group line and split the genes into columns
	word = ln.split()
	n_col = len(word)
	
	# if the number of columns are greater than 2 
	# which selects the genes of the TAG species which shears the gene groups with the rest of the species
	# we are removing the orthogroups which have n_col == 1 ; where they are "unassigned genes"
	tag_gene_count = 0

	if(n_col > 2):
		#search for genes which startes with the species-TAG 	
		for n in range(1, n_col):
			#count the number of TAG species in the orthogroup
			if(re.search(tag,word[n])):
				tag_gene_count = tag_gene_count + 1
				#add the gene-id to the list
				gene_id_list = gene_id_list + word[n] + "\t"
	#Check the number of TAG genes are above the theshold value ( = tag_no , default value is 5)	
	if(tag_gene_count > int(tag_no)):
		print("orthogroup: %s \t Total number of genes: %s \tNumber of TAG genes: %s" %(word[0],n_col-1,tag_gene_count))
		passing_gene_id_list = gene_id_list.strip()
	#passes the geneID list featuring only the TAG species	
	return(passing_gene_id_list)




'''
This will read the gene list and determine the longest and will wirte the FASTA sequence to a file
input:
	gene_list = gene list as a string
	f_file = FASTA file name of the species
	fasta_out = output FASTA file name
	length_flag = length flag to select the longest or shortest gene length (default longest)
output:
	Will write the longest sequence to the fasta_out file
'''
def select_gene_id(gene_list,f_file,fasta_out,length_flag):
	gene_records=[]
	gene_list = gene_list.lstrip(' ')
	gene = gene_list.split("\t")
	for g in gene:
		#reads the fasta file and find the length 
		for seq_record in SeqIO.parse(f_file, "fasta"):
			#match the gene_id to the fasta record;
			#then store it in a tuple
			 if(seq_record.id == g):
				print("match found  gene-id: %s  length: %s" %(seq_record.id,len(seq_record)))
				entry = (len(seq_record),seq_record.seq,seq_record.id,seq_record.description)
				gene_records.append(entry)
	if(gene_records):
		gene_records.sort(reverse=length_flag)
		print("sorted gene: %s  length: %s \n" %(gene_records[0][2],gene_records[0][0]))
		rec = SeqRecord(gene_records[0][1],
			id = gene_records[0][2],
			description = gene_records[0][3])
		SeqIO.write(rec, fasta_out, "fasta")
	return	
	



'''
The main program 
This accepts main arguements as 'ORTHO_GROUP_FILE', 'FASTA_FILE' and 'SPECIES_TAG' as main inputs.
The program selects the expanded genes in orthogroup using the 'SPECIES_TAG', and uses a default value
of 5 to select the expanded orthogroup family of genes. While this number can be changed using 
'NO_OF_TAG_SPECIES' value. 
The selected longest sequences will be written to a file name called sorted_genes.fasta while this value
can be changed using 'OUTPUT_FILE'.
Longest gene sequece is selected by the default boolean variable called -l, while it can be changed to 
select the smallest if -s flag is selected

usage: longestGene4m_ExpandedGeneFamilies.py       [-h]
                                                   [--orthofile ORTHO_GROUP_FILE]
                                                   [--fasta FASTA_FILE]
                                                   [--tag SPECIES_TAG]
                                                   [--no NO_OF_TAG_SPECIES]
                                                   [-l] [-s]
                                                   [--outfile OUTPUT_FILE]
                                                   [--version]

required arguments:
  --orthofile ORTHO_GROUP_FILE
                        ortho group file name
  --fasta FASTA_FILE    FASTA file name
  --tag SPECIES_TAG     gene identification tag for the species eg: TR is the
                        tag for TR22118|c0_g1_i1_14120|m.21703

optional arguments
  --no NO_OF_TAG_SPECIES
                        number of species in the orthogroup to be considered
                        in selecting the orthogroup
  -l                    boolean switch selects the longest gene length. If not
                        used default is set to select the longest
  -s                    boolean switch selects the shortest gene length. If
                        not used default is set to select the longest
  --outfile OUTPUT_FILE
                        output file name of the sorted genes. If not given it
                        will use the default name; sorted_genes.fasta
  --version             show program's version number and exit

'''

def main():
	#creates a parser object
	parser = argparse.ArgumentParser()
	#create action variables to hold the passed arguments
	#gets --orthofile       ortho_group_File        results.ortho_group_File 
	parser.add_argument('--orthofile', action='store', dest='ortho_group_File',
		help='ortho group file name')
	#gets --fasta           fasta_file              Fasta file of the main TAG species
	parser.add_argument('--fasta', action='store', dest='fasta_file',
		help='FASTA file name ')
	#gets --tag             species_tag             TAG name of the species in orthogroupfile
	parser.add_argument('--tag', action='store', dest='species_tag',
		help='gene identification tag for the species eg: TR is the tag for TR22118|c0_g1_i1_14120|m.21703')
	#gets --no              no_of_tag_species       number of species to consider in selecting the orthogroup (def=5)
	parser.add_argument('--no', action='store', default='5', 
		dest='no_of_tag_species',
		help='number of species in the orthogroup to be considered in selecting the orthogroup')
	
	parser.add_argument('-l', action='store_true', default=True,
		dest='gene_length',
		help='boolean switch selects the longest gene length. If not used default is set to select the longest')
	parser.add_argument('-s', action='store_false', default=True,
                dest='gene_length', 
                help='boolean switch selects the shortest gene length. If not used default is set to select the longest')
	#gets --outfile		output_file		name of the output file of the sorted genes fasta file
	parser.add_argument('--outfile', action='store', default='sorted_genes.fasta',
		dest='output_file',
		help='output file name of the sorted genes. If not given it will use the default name; sorted_genes.fasta ') 
	parser.add_argument('--version', action='version', version='%(prog)s 1.1')

	results = parser.parse_args()
	info()
	print 'ortho group file name 	=', results.ortho_group_File
	print 'fasta file name		=', results.fasta_file
	print 'species tag		=', results.species_tag
	print 'no of tag species        =', results.no_of_tag_species
	print 'sorted gene length	=', results.gene_length
	print 'output file name 	=', results.output_file
	print '\n\n'

	tag=results.species_tag
	#program checkes whether three main necessary arguments have been recived if not exit
	if(results.ortho_group_File and results.fasta_file and results.species_tag):
		#info()
		#open the Orthogroup file to get the genes which belong to each orthogroup
		file = open(results.ortho_group_File , "r")
		##open a file to write the sorted ortholog genes
		out_file = results.output_file
		fasta_out=open(out_file,"w")
		total = 0
		# read line by line in the orthogroup.txt file
		for line in file:
			#
			if(re.search(tag,line)):
				line = line.strip()
				#gets the geneID's of the filtered TAG genes
				g_list = orthogroup_expandedGenesInTagSpecies(line,tag,results.no_of_tag_species)
				#if the list is not empty then it will pass to get the longest gene seq from the list
				if(g_list):
					#pass the list of geneID's to get the longest seq and to write it to a file
					select_gene_id(g_list,results.fasta_file,fasta_out,results.gene_length)
	else:
		print 'Program',sys.argv[0] ,': : error: too few arguments'
		print '''\
                use -h to get the help file
                '''
		




main()

