#!/bin/bash
#$ -N muscle_job
#$ -M nasim.rahmatpour@uconn.edu
#$ -m bea
#$ -S /bin/bash
#$ -q highmem.q
#$ -cwd
#$ -pe smp 16
#$ -o muscle_$JOB_ID.out
#$ -e muscle_$JOB_ID.err

python muscle.py

