#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -c 15 
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o orthofinder_diamond_%j.out
#SBATCH -e orthofinder_diamond_%j.err

module load OrthoFinder/1.1.10
module load muscle
module load FastME
module load diamond
module load mcl
module load anaconda2/4.4.0
export PATH=/UCHC/LABS/Wegrzyn/OrthoFinder-1.1.10_source/orthofinder/:$PATH
/UCHC/LABS/Wegrzyn/OrthoFinder-1.1.10_source/orthofinder/orthofinder.py -f /home/CAM/nrahmatpour/Nasim/Haploid.Gametophyte/Orthofinder/all_mosses/new/all_3 -S diamond -t 15 
