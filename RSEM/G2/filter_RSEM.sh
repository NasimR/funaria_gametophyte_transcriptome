#!/bin/bash
#$ -N filter_trinity__turtle
#$ -M nasim.rahmatpour@uconn.edu
##$ -q highmem.q
#$ -m bea
#$ -S /bin/bash
#$ -cwd
##$ -pe smp 4 
#$ -o RSEMtrinity_$JOB_ID.out
#$ -e RSEMtrinity_$JOB_ID.err

module load trinity/2.2.0

/opt/bioinformatics/trinity2/util/filter_fasta_by_rsem_values.pl \
	--rsem_output=/archive/Nasim/NextSeq/RSEM/RSEM_replicate/G2/RSEM.isoforms.results \
	--fasta=/archive/Nasim/NextSeq/RSEM/RSEM_replicate/G2/G2.trinity.fasta \
	--output=/archive/Nasim/NextSeq/RSEM/RSEM_replicate/G2/cutoff_FPKM0.5/G2_fpkm_filtered.fasta \
	--fpkm_cutoff=0.5 \
	--isopct_cutoff=1

