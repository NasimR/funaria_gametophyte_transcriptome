#!/bin/bash
#$ -N get_genepairs
#$ -M nasim.rahmatpour@uconn.edu
#$ -q all.q
#$ -m bea
#$ -S /bin/bash
#$ -cwd
#$ -pe smp 1
#$ -o getgenepairs_$JOB_ID.out
#$ -e getgenepairs_$JOB_ID.err

python get_genepairs.py gene_ID_lower_limit.txt  G1_fpkm_filtered.fasta.transdecoder.cds
