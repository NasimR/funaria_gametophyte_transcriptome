import argparse

_input_path = ""

def init_argparse():
    global _input_path
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', action='store', dest='input', help='Path to input file', type=str)
    args = parser.parse_args()
    _input_path = args.input


def format_seq():
    # read into memory (not to large)
    outpath = _input_path + "alt"
    out_file = open(outpath, 'w')
    lines = []
    with open(_input_path, 'r') as input_file:
        for line in input_file:
            if not line:
                continue
            if line[0] == '>':
                line = line[0:line.find(" ")]
                line += "\n"
            lines.append(line)
    out_file.writelines(lines)
    out_file.close()


init_argparse()
format_seq()
