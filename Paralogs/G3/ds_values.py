#!/usr/bin/env python

#########################################################################################
#command python ds_values.py > dnds_results_summary.txt					#
#########################################################################################
import glob, os, re

for file in glob.glob("*.results.txt"):
        f=open(file)
        for line in f:
                if re.search("dN/dS=", line):
                        basename=os.path.basename(file)
                        base=os.path.splitext(basename)[0]
                        base1=os.path.splitext(base)[0]
                        base2=os.path.splitext(base1)[0]
                        base3=os.path.splitext(base2)[0]
                        base4=base3 + ".*"
                        #print(base4)
                        #t= 0.1714  S=   259.1  N=   757.9  dN/dS=  0.1686  dN = 0.0253  dS = 0.1502
                        #discards the newline charactor
                        line=line.rstrip()
                        #word=line.split()
                        line = re.sub('[=]', '', line)
                        word=line.split()
                        #['t', '50.0000', 'S', '97.8', 'N', '286.2', 'dN/dS', '0.0018', 'dN', '0.1163', 'dS', '65.0825']

                        dnds_value = float(word[7])
                        print line + "\t" + file
                        #print "dN/dS value: " + str(dnds_value)
                        ds_value = float(word[11])
                        #print "dS value: " + str(ds_value)
                        dn_value = float(word[9])
                        #print "dN value: " + str(dn_value)
                        #print("File : %s  dN/dS: %s  dS: %s  dN: %s" %(file, dnds_value,ds_value,dn_value))
        f.close()

