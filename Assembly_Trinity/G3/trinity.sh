#!/bin/bash
#$ -N trinity_job 
#$ -M nasim.rahmatpour@uconn.edu
#$ -m bea
#$ -S /bin/bash
#$ -q highmem.q 
#$ -cwd
#$ -pe smp 16 
# Specify the output file
#$ -o trinity_$JOB_ID.out
# Specify the error file
#$ -e trinity_$JOB_ID.err

module load trinity/2.0.6
Trinity --seqType fq --left /scratch2/Nasim/G3/cat.G3.R1.fastq --right /scratch2/Nasim/G3/cat.G3.R2.fastq --min_contig_length 300 --output /scratch2/Nasim/G3/trinity_run --full_cleanup --max_memory 150G --CPU 16
