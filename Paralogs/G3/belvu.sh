#!/bin/bash
#$ -N belvu_N
#$ -M nasim.rahmatpour@uconn.edu
#$ -m bea
#$ -S /bin/bash
#$ -cwd
#$ -pe smp 1
#$ -o $JOB_ID.out
#$ -e $JOB_ID.err
module load seqtools/4.42.1
python belvu.py

