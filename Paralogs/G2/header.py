#!/usr/bin/env python

import glob, os

for fasta_file in glob.glob("*.afa"):
    new_file = fasta_file + ".header"
    command = "awk '{print /^>/ ? $1 : $0}' <%s >%s" % (fasta_file, new_file)
    print "executing: " + command
    os.system(command)

