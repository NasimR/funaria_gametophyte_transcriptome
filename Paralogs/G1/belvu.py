#!/usr/bin/env python

#########################################################################
# Coded by Neranjan V. Perera						#
# University of Connecticut						#
#									#
#########################################################################

import glob, os, re

belvu_options = " -c -G"

for afa_file in glob.glob('*.afa'):
    base = os.path.basename(afa_file)
    abs_base=base[:-8]
    base_txt=os.path.splitext(base)[0]
    out_file = base_txt + '.belvu'
    command = "belvu %s %s > %s" %(belvu_options, afa_file, out_file)
    print "excuting: " + command
    os.system(command)
    
