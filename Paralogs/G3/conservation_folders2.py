#!/usr/bin/env python

#################################################################################
# 	Coded by: Neranjan Perera						#
#	conservation_folders.py [ filter value ] [ folder Name]			#
#	eg: conservation_folders.py 3.5 conservation_folder_3p5			#
#										#		
#	It takes the filter value (which is in the belvu file)			#
#	and use it to catogerize the alignments according to 			#
#	the 'Average conservation' value. 					#
#	Then it copies the *.afa *.belvu files to the "FOLDER"			#
#	(Actually it will copy all the files that has the base name <File.fasta>#
#################################################################################
import glob, os, re, sys


#gets the filter value
c_value=sys.argv[1]
#gets the new folder name
get_folder=sys.argv[2]

#gets the current path
folder=os.getcwd()
new_folder=os.path.join(folder,get_folder)

#creates a "NEW FOLDER"
os.mkdir(new_folder, 0755 )
print "New Folder is created !!!"
count=0


for filename in glob.glob('*.belvu'):
	f=open(filename)
	
	for line in f:
		if re.search("Average conservation", line):
			#discards newline charactor
			line=line.rstrip()
			word=line.split()
			value=word[3]
			#print value
			basename=os.path.basename(filename)
			base=os.path.splitext(basename)[0]
			
			if value >= c_value:
				print "Value is Greater : %s file: %s" %(value, filename)
				all_file=base + ".*"
				command2="cp %s %s" %(all_file, new_folder)
				os.system(command2)
				count=count+1


	f.close()

print "count: %d" %(count)
