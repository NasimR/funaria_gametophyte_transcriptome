#!/bin/bash
#$ -N conserv3p5
#$ -M nasim.rahmatpour@uconn.edu
#$ -m bea
#$ -S /bin/bash
#$ -cwd
#$ -pe smp 1
#$ -o $JOB_ID.out
#$ -e $JOB_ID.err
python conservation_folders2.py 3.5 conservation_value_3p5
