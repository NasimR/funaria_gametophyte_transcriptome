#!/bin/bash
#$ -N header_job
#$ -M nasim.rahmatpour@uconn.edu
#$ -m bea
#$ -S /bin/bash
#$ -q highmem.q
#$ -cwd
#$ -pe smp 16
#$ -o header_$JOB_ID.out
#$ -e header_$JOB_ID.err
python header.py

