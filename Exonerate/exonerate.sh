#!/bin/bash
#BATCH -J exonerate

#SBATCH -N 1

#SBATCH -n 1

#SBATCH -c 1

#SBATCH --mail-user=nasim.rahmatpour@uconn.edu

#SBATCH --mail-type=ALL

#SBATCH -o exonearate-%j.output

#SBATCH -e exonerate-%j.error

#SBATCH -p general

#SBATCH --qos=general

#SBATCH --mem=50G

module load exonerate/2.4.0

exonerate --query /home/CAM/nrahmatpour/pepfiles/splitfasta/Funaria.fasta_chunk_00000"$1" --querytype protein --target /home/CAM/nrahmatpour/pepfiles/Ppatensoftmasked/Ppatens_318_v3.softmasked.fa --targettype dna --model protein2genome --percent 70 --score 500 -n 1 --minintron 10  --verbose 1 > /home/CAM/nrahmatpour/pepfiles/troubleshoot/output/Funaria_"$1".gff

if [ $? -eq 0 ]; then
	echo "Funaria.fasta_chunk_00000$1" $1  SUCCESSFUL >> file_status_OK.txt 
else
	echo "Funaria.fasta_chunk_00000$1" $1  FAIL >> file_status_FAIL.txt
fi
