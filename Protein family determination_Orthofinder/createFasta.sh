#!/bin/bash
#SBATCH --job-name=createFata
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --mem=128M
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o createFasta_%j.out
#SBATCH -e createFasta_%j.err
python createFasta.py --fasta Funaria.cds --path /home/CAM/nrahmatpour/Nasim/Haploid.Gametophyte/Orthofinder/all_mosses/new/Results_Aug26/Unassigned/Funaria --nameList Funaria_unassigned.txt --pathList /home/CAM/nrahmatpour/Nasim/Haploid.Gametophyte/Orthofinder/all_mosses/new/Results_Aug26/Unassigned/Funaria --out Funaria_unassigned.cds
