#!/bin/bash
#$ -S /bin/bash
#$ -pe smp 4 
#$ -q course.q
#$ -M nasim.rahmatpour@uconn.edu 

# Specify mailing options: b=beginning, e=end, s=suspended, n=never, a=abortion
#$ -m esa

# Specify the output file
#$ -o codeml_$JOB_ID.out

# Specify the error file
#$ -e codeml_$JOB_ID.err
#$ -cwd
for file in *; do
	if [[ "$file" == *header* ]]
	then
		shortfile=${file/.header/}
		sed -i "1s/.*/seqfile = $file/" codeml.ctl
		sed -i "3s/.*/outfile = $shortfile.results.txt/" codeml.ctl
        	codeml codeml.ctl
	fi
done
