#!/bin/bash
#$ -N bowtie_build
#$ -M nasim.rahmatpour@uconn.edu         # 
#$ -m bea
#$ -S /bin/bash
#$ -cwd
#$ -pe smp 6            # cores requested
#$ -o $HOME/bowtie_build_$JOB_ID.out
#$ -e $HOME/bowtie_build_$JOB_ID.err

bowtie-build /archive/Nasim/NextSeq/RSEM/RSEM_replicate/G2/G2.trinity.fasta  /archive/Nasim/NextSeq/RSEM/RSEM_replicate/G2/G2.trinity.fasta.bowtie

