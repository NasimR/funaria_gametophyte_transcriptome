#!/bin/bash
#SBATCH --job-name=Expanded_genefamiles
#SBATCH -c 15 
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o Expanded_genefamiles_%j.out
#SBATCH -e Expanded_genefamiles_%j.error

module load biopython/1.70

python longestGene4m_ExpandedGeneFamilies.py \
--orthofile  Orthogroups_Funaria_expanded.txt \
--fasta /home/CAM/nrahmatpour/Nasim/Haploid.Gametophyte/Orthofinder/all_mosses/new/Results_Aug26/Expanded/Funaria/Funaria.fasta \
--tag TR \
--no 0 \
--outfile Funaria_expanded_orthogroup.fasta > log
