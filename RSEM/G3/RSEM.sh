#!/bin/bash
#$ -N RSEM_turtle
#$ -M nasim.rahmatpour@uconn.edu         # 
#$ -m bea
#$ -S /bin/bash
#$ -cwd
#$ -q molfind.q
#$ -pe smp 4            # cores requested
#$ -o $HOME/log/RSEMtrinity_$JOB_ID.out
#$ -e $HOME/log/RSEMtrinity_$JOB_ID.err

module load trinity/2.2.0

/opt/bioinformatics/trinity2/util/align_and_estimate_abundance.pl --transcripts /archive/Nasim/NextSeq/RSEM/RSEM_replicate/G3/G3.trinity.fasta --seqType fq --left /archive/Nasim/NextSeq/catfiles/cat.G3/cat.G3.R1.fastq --right /archive/Nasim/NextSeq/catfiles/cat.G3/cat.G3.R2.fastq --est_method RSEM --aln_method bowtie --thread_count 4 --prep_reference --output_dir /archive/Nasim/NextSeq/RSEM/RSEM_replicate/G3/
 --output_prefix G3 # give a prefix for your output file.
