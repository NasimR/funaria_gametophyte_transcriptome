#!/bin/bash
#SBATCH --job-name=gmap
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o gmap_%j.out
#SBATCH -e gmap_%j.err

module load gmap/2017-03-17
gmap -D /UCHC/LABS/Wegrzyn/Funaria_Genome/Funaria_gmap_index/ -d Funaria_index \
        -A /UCHC/LABS/Wegrzyn/Funaria_Genome/gmap_transcriptome_to_genome/coverage95/gmap98/Funaria_transcriptome.fasta \
        -t 8 \
        --min-trimmed-coverage=0.95 --min-identity=0.98 -n 0 -f gff3_gene > BS-Physco3.clustered-gmap80.gff3 -S

